/* shader.vs */
#version 330
precision mediump float;

attribute vec3 vCoord;
attribute vec2 vtCoord;
attribute vec3 vnCoord;

uniform mat4 mMat;	/* modelview  */
uniform mat4 vMat;	/* viewport   */
uniform mat4 pMat;	/* projection */

varying vec2 vt;    /* texture coordinate */
varying vec3 vn;    /* vertex normal      */

void main()
{
    gl_Position = pMat*vMat*mMat*vec4(vCoord, 1.0);
    vt = vtCoord; /* rotation should be applied */
    vn = vnCoord;
}
