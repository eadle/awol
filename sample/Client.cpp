// Client.cpp -- Client class implementation
#include "Client.hpp"

// default constructor
Client::Client()
{
	// where you are
	// /*LR.obj*/x0 = glm::vec3(25.212309f, 1.011917f, 19.822801);
	x0 = glm::vec3(15075.40429f, -5466.214844f, 15107.032227f);
	// where you are looking
	// /*LR.obj*/x1 = glm::vec3(-0.999297f, 0.037490f, 0.000111f);
	x1 = glm::vec3(-0.713659f, -0.161788f, -0.681554f);
	// definition of up
	up = glm::vec3(0.0f, 1.0f, 0.0f);
	recalc_orth();
	speed = SPEED;
	sprint = false;
}

// Calculate the vector orthogonal to x1 and up. Normalize.
void Client::recalc_orth()
{
	orth = glm::normalize(glm::cross(x1, up));
}

// Set a new viewing vector.
void Client::set_x1(glm::vec3 tx1)
{
	x1 = glm::normalize(tx1);
}

// Set a new definition of up.
void Client::set_up(glm::vec3 tup)
{
	up = glm::normalize(tup);
}

// set sprint state
void Client::set_sprint(bool state)
{
	if (state)
		speed = 5.0f*SPEED;
	else
		speed = SPEED;
}

// attempt to move right
void Client::moveR() { x0 += speed*orth; }
// attempt to move left
void Client::moveL() { x0 -= speed*orth; }
// attempt to move forwards
void Client::moveF() { x0 += speed*x1; }
// attempt to move backwards
void Client::moveB() { x0 -= speed*x1; }
// attempt to move forwards and right
void Client::moveFR() { x0 += speed*(x1+orth); }
// attempt to move forwards and left
void Client::moveFL() { x0 += speed*(x1-orth); }
// attempt to move backwards and right
void Client::moveBR() { x0 -= speed*(x1-orth); } 
// attempt to move backwards and left
void Client::moveBL() { x0 -= speed*(x1+orth); } 

/*=========================================================
Return the viewport glm::mat4 -- the 'V' in "MVP"
===========================================================*/
glm::mat4 Client::get_viewport_matrix() const
{
/*
	fprintf(stdout,
			"where you are: %f, %f, %f,\n"
			"where you're looking: %f, %f, %f\n\n",
			x0[0], x0[1], x0[2],
			x1[0], x1[1], x1[2]);	
*/
	return glm::lookAt(
		glm::vec3(x0[0], x0[1], x0[2]),
		glm::vec3(x1[0]+x0[0], x1[1]+x0[1], x1[2]+x0[2]),
		glm::vec3(up[0], up[1], up[2])
	);
}

/*=========================================================
Might be useful information to pass to a server. *hint hint*
For renderering any other Clients.
===========================================================*/
// return the Client's position glm::vec3
glm::vec3 Client::get_x0() const { return x0; }
// return the Client's viewing direction glm::vec3
glm::vec3 Client::get_x1() const { return x1; }
// return the Client's definition of up
glm::vec3 Client::get_up() const { return up; }
// return the Client's side direction
glm::vec3 Client::get_orth() const { return orth; }
