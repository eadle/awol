// Mesh.cpp
#include "Mesh.hpp"

using namespace glm;

Mesh::Mesh(void)
{
    // Modelview attributes:
    this->translation = vec3(0.0f);
    this->rotation = vec3(0.0f);
    this->scale = vec3(1.0f);

    // These are set when setShader is called.
    this->shaderID = GL_INVALID_VALUE;
    this->mMatHandle = GL_INVALID_VALUE;
    this->vCoordHandle = GL_INVALID_VALUE;
    this->vtCoordHandle = GL_INVALID_VALUE;
    this->vnCoordHandle = GL_INVALID_VALUE;
    this->texSamplerHandle = GL_INVALID_VALUE;
    this->mtlHandle = GL_INVALID_VALUE;

    // Haven't loaded it.
    this->model = NULL;
}

void Mesh::load(const char* objFile)
{
    this->model = awol_create_model(objFile);
    // If model was successfully loaded:
    if (this->model)
    {
        loadTextures();  // load the textures
        createBuffers(); // create the vertex buffers
    }
}

Mesh::~Mesh(void)
{
    if (NULL != model)
    {
        // Destroy the material buffers:
        if (GL_TRUE == glIsTexture(this->tbo[0]))
            glDeleteTextures(this->model->max_mtl, this->tbo);
        delete [] this->tbo;
        this->tbo = NULL;

        // Destroy the vertex buffers:
        if (GL_TRUE == glIsBuffer(this->vbo[0]))
            glDeleteBuffers(this->model->max_chunk, this->vbo);
        delete [] this->vbo;
        this->vbo = NULL;

        // Destroy the model:
        awol_destroy_model(this->model);
        this->model = NULL;
    }
}

void Mesh::loadTextures(void)
{
    this->tbo = new GLuint[this->model->max_mtl];
    glGenTextures(this->model->max_mtl, this->tbo);
    glActiveTexture(GL_TEXTURE0);

    int width, height;
    unsigned char* img;
    for (int mi = 0; mi < model->max_mtl; ++mi)
    {
        // Load map_Kd for each material:
        img = SOIL_load_image(model->mtl[mi]->map_Kd,
                              &width, &height, 0,
                              SOIL_LOAD_RGBA);
        // SOIL status:
        fprintf(stdout, "SOIL: %s : %s\n",
                model->mtl[mi]->map_Kd,
                SOIL_last_result());

        glBindTexture(GL_TEXTURE_2D, this->tbo[mi]);
        glTexParameteri(GL_TEXTURE_2D,
                        GL_TEXTURE_WRAP_S,
                        GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D,
                        GL_TEXTURE_WRAP_T,
                        GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D,
                        GL_TEXTURE_MAG_FILTER,
                        GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D,
                        GL_TEXTURE_MIN_FILTER,
                        GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                     width, height, 0, GL_RGBA,
                     GL_UNSIGNED_BYTE, img);
        glUniform1i(this->texSamplerHandle, 0);
        glBindTexture(GL_TEXTURE_2D, 0);

        free(img);
    }
}

// Create a vertex buffer object for each draw chunk!
void Mesh::createBuffers(void)
{
    int max_chunk = this->model->max_chunk;
    float* v = this->model->vertices;
    float* vt = this->model->textures;
    float* vn = this->model->normals;

    // Allocate VBO handle memory:
    this->vbo = new GLuint[max_chunk];
    glGenBuffers(max_chunk, this->vbo);

    size_t sof = sizeof(float);
    // fpt - floats per triangle
    size_t fpt = (NULL != vn) ? 24 : 15;

    // For each draw chunk, create a VBO:
    for (int ci = 0; ci < max_chunk; ++ci)
    {
        // Convenience:
        struct awol_chunk* chunk = this->model->chunk[ci];
        int* tri_index = chunk->tri_index;
        int max_tri = chunk->max_tri;
        // Allocate temporary VBO memory: 
        GLfloat* vboData = new GLfloat[fpt*max_tri];
        // Create a series of triangles as the VBO:
        for (int ti = 0; ti < max_tri; ++ti)
        {
            // Case where normals existed in OBJ file:
            if (NULL != vn)
            {
                // First vertex:
                memcpy(vboData+fpt*ti,     v+3*tri_index[9*ti],   3*sof);
                memcpy(vboData+fpt*ti+3,  vt+2*tri_index[9*ti+1], 2*sof);
                memcpy(vboData+fpt*ti+5,  vn+3*tri_index[9*ti+2], 3*sof);
                // Second vertex:
                memcpy(vboData+fpt*ti+8,   v+3*tri_index[9*ti+3], 3*sof);
                memcpy(vboData+fpt*ti+11, vt+2*tri_index[9*ti+4], 2*sof);
                memcpy(vboData+fpt*ti+13, vn+3*tri_index[9*ti+5], 3*sof);
                // Third vertex:
                memcpy(vboData+fpt*ti+16,  v+3*tri_index[9*ti+6], 3*sof);
                memcpy(vboData+fpt*ti+19, vt+2*tri_index[9*ti+7], 2*sof);
                memcpy(vboData+fpt*ti+21, vn+3*tri_index[9*ti+8], 3*sof);
            }
            // Case where only vertices and texture coordinates:
            else
            {
                // First vertex:
                memcpy(vboData+fpt*ti,     v+3*tri_index[6*ti],   3*sof);
                memcpy(vboData+fpt*ti+3,  vt+2*tri_index[6*ti+1], 2*sof);
                // Second vertex:
                memcpy(vboData+fpt*ti+5,   v+3*tri_index[6*ti+2], 3*sof);
                memcpy(vboData+fpt*ti+8,  vt+2*tri_index[6*ti+3], 2*sof);
                // Third vertex:
                memcpy(vboData+fpt*ti+10,  v+3*tri_index[6*ti+4], 3*sof);
                memcpy(vboData+fpt*ti+13, vt+2*tri_index[6*ti+5], 2*sof);
            }
        }
        // Make the current draw chunk buffer:
        glBindBuffer(GL_ARRAY_BUFFER, this->vbo[ci]);
        glBufferData(GL_ARRAY_BUFFER,
                     fpt*max_tri*sof,
                     vboData,
                     GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Delete the VBO memory:
        delete [] vboData;
    } 
}

// The model has vertices, textures and normals.
void Mesh::draw(void)
{
    if (!this->model) return;
    // Set modelview uniform:
    mat4 modelview = this->getModelview();
    glUniformMatrix4fv(mMatHandle, 1, GL_FALSE, value_ptr(modelview));
    // Process the draw chunks one at a time:
    for (int ci = 0; ci < this->model->max_chunk; ++ci)
    {
        struct awol_chunk* chunk = this->model->chunk[ci];
        glBindTexture(GL_TEXTURE_2D, this->tbo[chunk->mtl_index]);
        glBindBuffer(GL_ARRAY_BUFFER, this->vbo[ci]);
        // Send current mtl index to fragment shader:
        glUniform1i(mtlHandle, chunk->mtl_index);
        // Determine stride between attributes:
        size_t stride = (this->model->normals) ? 8 : 5;
        stride *= sizeof(GLfloat);
        // Setup shader attributes:
        glEnableVertexAttribArray(vCoordHandle);
        glVertexAttribPointer(vCoordHandle,
                              3,
                              GL_FLOAT,
                              GL_FALSE,
                              stride,
                              0);
        glEnableVertexAttribArray(vtCoordHandle);
        glVertexAttribPointer(vtCoordHandle,
                              2,
                              GL_FLOAT,
                              GL_FALSE,
                              stride,
                              (const GLvoid*)(3*sizeof(GLfloat)));
        // Pass normals if they existed in OBJ file:
        if (this->model->normals)
        {
            glEnableVertexAttribArray(vnCoordHandle);
            glVertexAttribPointer(vnCoordHandle,
                                  3,
                                  GL_FLOAT,
                                  GL_FALSE,
                                  stride,
                                  (const GLvoid*)(5*sizeof(GLfloat)));
        }
        // Draw the triangles:
        glDrawArrays(GL_TRIANGLES, 0, 3*chunk->max_tri);
    } 
    // Done drawing -- disable shader attributes:
    glDisableVertexAttribArray(vCoordHandle);
    glDisableVertexAttribArray(vtCoordHandle);
    glDisableVertexAttribArray(vnCoordHandle);
}

// The model needs some information to be drawn.
void Mesh::setShader(GLuint shaderID)
{
    this->shaderID = shaderID;
    this->mMatHandle = 
        glGetUniformLocation(this->shaderID, UNIFORM_MODELVIEW);
    this->vCoordHandle = 
        glGetAttribLocation(this->shaderID, ATTRIBUTE_VCOORD);
    this->vtCoordHandle = 
        glGetAttribLocation(this->shaderID, ATTRIBUTE_VTCOORD);
    this->vnCoordHandle =
        glGetAttribLocation(this->shaderID, ATTRIBUTE_VNCOORD);
    this->texSamplerHandle =
        glGetUniformLocation(this->shaderID, UNIFORM_TEXSAMPLER);
    this->mtlHandle =
        glGetUniformLocation(this->shaderID, UNIFORM_MTL);
}

// Set the model's render translation.
void Mesh::setTranslation(const glm::vec3& translation)
{
    this->translation = translation;
}

// Set the model's render rotation.
void Mesh::setRotation(const glm::vec3& rotation)
{
    this->rotation = rotation;
}

// Set the model's render scale.
void Mesh::setScale(const glm::vec3& scale)
{
    this->scale = scale;
}

// Return the modelview matrix:
glm::mat4 Mesh::getModelview(void) const
{
    using namespace glm;
    // Not handling rotation yet, gimble lock problems. Quaternions?
    mat4 modelview = translate(mat4(1.0f), this->translation);
    modelview = glm::scale(modelview, this->scale);
    return modelview;
}

// Return the awol_model structure:
struct awol_model* Mesh::getModel(void) const
{
    return this->model;
}
