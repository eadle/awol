// testAWOL.cpp
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_interpolation.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "constants.h"
#include "loadShaders.h"
#include "Client.hpp"
#include "Mesh.hpp"

using namespace glm;

static struct game 
{
    Client user; 
    Mesh* racetrack;     
    GLuint shaderID;
}demo;

// Key state buffer:
static bool keyState[256] = {false};
void keyPressed(unsigned char key, int mx, int my) { keyState[key] = true; }
void keyUp(unsigned char key, int mx, int my) { keyState[key] = false; }

void quit()
{
    glUseProgram(0);
    delete demo.racetrack;
    if (GL_TRUE == glIsProgram(demo.shaderID))
        glDeleteProgram(demo.shaderID);
    fprintf(stdout, "Thanks for trying AWOL!\n");
    exit(EXIT_SUCCESS);
}

// Move user around:
void keyOperations()
{
    if (keyState['q']) quit();
    // test if user is sprinting
    if (keyState[32])
        demo.user.set_sprint(true);
    else
        demo.user.set_sprint(false);
    // use key states to decide movement direction
    if (keyState[UP] && keyState[LEFT] && keyState[DOWN])
        demo.user.moveL();
    else if (keyState[UP] && keyState[RIGHT] && keyState[DOWN])
        demo.user.moveR();
    else if (keyState[UP] && keyState[LEFT] && keyState[RIGHT])
        demo.user.moveF();
    else if (keyState[DOWN] && keyState[LEFT] && keyState[RIGHT])
        demo.user.moveB();
    else if (keyState[UP] && keyState[LEFT])
        demo.user.moveFL();
    else if (keyState[UP] && keyState[RIGHT])
        demo.user.moveFR();
    else if (keyState[DOWN] && keyState[LEFT])
        demo.user.moveBL();
    else if (keyState[DOWN] && keyState[RIGHT])
        demo.user.moveBR();
    else if (keyState[UP])
        demo.user.moveF();
    else if (keyState[RIGHT])
        demo.user.moveR();
    else if (keyState[DOWN])
        demo.user.moveB();
    else if (keyState[LEFT])
        demo.user.moveL();    
}

void processMousePassiveMotion(int mx, int my)
{
    static GLfloat ltime = 0.0f;
    GLfloat ctime = glutGet(GLUT_ELAPSED_TIME)/1000.0f;
    static int xlast = -1, ylast = -1;
    if (xlast == -1 || ylast == -1 || ctime-ltime > 0.25f)
    {
        xlast = mx;
        ylast = my;
    }
    // difference from last mouse position
    int yChange = ylast - my;
    int xChange = xlast - mx;

    if (mx != xlast)
    {
        // determine rotation about Y-axis
        GLfloat beta = 0.0125*xChange;
        mat4 rotation = axisAngleMatrix(vec3(0.0f, 1.0f, 0.0f), beta);
        demo.user.set_x1(vec3(rotation*vec4(demo.user.get_x1(), 1.0f)));
        demo.user.set_up(vec3(rotation*vec4(demo.user.get_up(), 1.0f)));
        demo.user.recalc_orth();
    }
    
    // find angle between x1 and j-hat
    GLfloat num = dot(demo.user.get_x1(), vec3(0.0f, 1.0f, 0.0f));
    GLfloat denom = length(demo.user.get_x1());
    GLfloat theta = acos(num/denom)*180.0/M_PI;

    // TO DO: don't look past vec3(0.0f, -1.0f, 0.0f) and vec3(0.0f, 1.0f, 0.0f)
    if (my != ylast)
    {
        // determine rotation about A-axis
        GLfloat alpha = 0.0125*yChange;
        mat4 rotation = axisAngleMatrix(demo.user.get_orth(), alpha);    
        demo.user.set_x1(vec3(rotation*vec4(demo.user.get_x1(), 1.0f)));
        demo.user.set_up(vec3(rotation*vec4(demo.user.get_up(), 1.0f)));
    }

    xlast = mx;
    ylast = my;
    ltime = ctime;
}

bool timeForTick()
{
    static GLfloat t0 = 0.0f;
    GLfloat t1 = glutGet(GLUT_ELAPSED_TIME)/1000.0f;
    if (t1-t0 >= 1.0f/60.0f)
    {   
        t0 = t1; 
        return true;
    }   
    else return false;
}

void renderFrame()
{
    if (!timeForTick()) return;
    keyOperations();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // For the camera:
    GLuint vMatHandle = glGetUniformLocation(demo.shaderID, UNIFORM_VIEWPORT);
    mat4 viewport = demo.user.get_viewport_matrix();
    glUniformMatrix4fv(vMatHandle, 1, GL_FALSE, value_ptr(viewport));
    // Draw the racetrack:
    demo.racetrack->draw();
    glutSwapBuffers();
}

void init()
{
    // Compile and link the shader programs:
    GLuint shaders[] = {
        createShader(GL_VERTEX_SHADER, "./sample/shader.vs"),
        createShader(GL_FRAGMENT_SHADER, "./sample/shader.fs")
    };
    demo.shaderID = createProgram(shaders, 2);
    glUseProgram(demo.shaderID);

    // Setup an OBJ to render:
    demo.racetrack = new Mesh();
    demo.racetrack->setShader(demo.shaderID);
    demo.racetrack->setTranslation(vec3(13570.0f, -6655.0f, 14325.0f));
    demo.racetrack->setScale(vec3(0.01f, 0.01f, 0.01f));
    // Load one of the racetracks:
    //demo.racetrack->load("./assets/waluigistadium/waluigistadium.obj");
    //demo.racetrack->load("./assets/wariocolosseum/wariocolosseum.obj");
    //demo.racetrack->load("./assets/daisycruiser/cruiseship.obj");
    //demo.racetrack->load("./assets/luigicircuit/luigicircuit.obj");
    //demo.racetrack->load("./assets/bowserscastle/bowserscastle.obj");
    fprintf(stdout, "Please see \"model-resources.txt\" in assets folder.\n");

    // OpenGL settings:
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);
    //glutFullScreen();
}

void reshapeScene(int w, int h)
{
    glViewport(0, 0, GLuint(w), GLuint(h));
    // Set perspective uniform:
    mat4 pMat = perspective(45.0f, float(w)/float(h), 2.0f, 50000.0f);
    GLuint pMatHandle = glGetUniformLocation(demo.shaderID, UNIFORM_PROJECTION);
    glUniformMatrix4fv(pMatHandle, 1, GL_FALSE, value_ptr(pMat));
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(WIDTH, HEIGHT);
    glutCreateWindow("AWOL Example");

    // initialize glew
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        // problem: glewInit failed
        fprintf(stderr, "GLEW ERROR: %s\n", glewGetErrorString(err));
        exit(1);
    }
    else fprintf(stdout, "GLEW initialized successfully!\n");

    // initialize scene
    init();

    // main program loop    
    glutDisplayFunc(renderFrame);
    glutReshapeFunc(reshapeScene);
    glutIdleFunc(renderFrame);
    glutKeyboardFunc(keyPressed);
    glutKeyboardUpFunc(keyUp);
    glutPassiveMotionFunc(processMousePassiveMotion);
    glutMainLoop();
    
    return 0;
}
