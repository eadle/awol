// Client.hpp -- Client class declaration
#ifndef CLIENT_H_
#define CLIENT_H_
#define SPEED 2 

#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Client
{
public:
	Client(void);

	// Client movement
	void moveR(void); 	// move right
	void moveL(void); 	// move left
	void moveF(void); 	// move forwards
	void moveB(void); 	// move backwards
	void moveFR(void); 	// move forwards and right
	void moveFL(void); 	// move forwards and left
	void moveBR(void); 	// move backwards and right
	void moveBL(void);	// move backwards and left

	// getters
	glm::mat4 get_viewport_matrix(void) const;	// viewport matrix
	glm::vec3 get_x0(void) const;				// position vector
	glm::vec3 get_x1(void) const;				// viewing vector 
	glm::vec3 get_up(void) const;				// up vector 
	glm::vec3 get_orth(void) const;				// side vector

	// setters
	void set_sprint(bool state);
	void set_x1(glm::vec3 tx1);	
	void set_up(glm::vec3 tup);	
	void recalc_orth(void);
private:
	// orientation components
	glm::vec3 x0;	// position
	glm::vec3 x1;	// viewing direction
	glm::vec3 up;	// up definition
	glm::vec3 orth;	// x1 cross-product up

	// other components
	float speed;
	bool sprint;
};

#endif // CLIENT_H_
