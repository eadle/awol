// Mesh.hpp
#ifndef MESH_HPP_
#define MESH_HPP_
#include <cstdio>
#include <cstdlib>
#include <GL/glew.h>
#include <GL/freeglut.h>
// OpenGL Mathematics Library
#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
// Simple OpenGL Image Loader
#include <SOIL/SOIL.h>
// Another Wavefront OBJ Loader
#include "../awol/awol.h"
#include "constants.h"

class Mesh
{
public:
    Mesh(void);
    ~Mesh(void);
    void load(const char*);
    void draw(void);
    void setShader(GLuint);
    void setTranslation(const glm::vec3&);
    void setRotation(const glm::vec3&);
    void setScale(const glm::vec3&);
    struct awol_model* getModel(void) const;
    glm::mat4 getModelview(void) const;
private:
    // Model attributes:
    struct awol_model* model;
    glm::vec3 translation;
    glm::vec3 rotation;
    glm::vec3 scale;
    // OpenGL identifiers:
    GLuint shaderID;           // the shader program being used 
    GLuint* vbo;                // vertex buffer object handles
    GLuint* tbo;                // texture buffer object handles
    GLuint mMatHandle;          // modelview uniform handle 
    GLuint vCoordHandle;        // geometric vertex attribute
    GLuint vtCoordHandle;       // texture coordinate attribute 
    GLuint vnCoordHandle;       // vertex normal attribute
    GLuint texSamplerHandle;    // texture sampler uniform
    GLuint mtlHandle;           // material index being used
    // Helpers:
    void draw_vvtvn(void);
    void draw_vvt(void);
    void loadTextures(void);
    void createBuffers(void);
};

#endif
