/* shader.fs */
#version 330
precision mediump float;

uniform sampler2D tex;
varying vec2 vt;        /* texture coordinate */
varying vec3 vn;        /* vertex normal      */

void main()
{
    vec4 color = texture(tex, vec2(vt.s, 1.0-vt.t));
    if (0.0 == color.a) discard;
    else gl_FragColor = color;
}
