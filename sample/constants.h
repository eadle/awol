#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#ifndef M_PI
    #define M_PI 3.14159265359
#endif

/* Screen Dimensions */
#define WIDTH  800
#define HEIGHT 500

/* Shader Constants */
#define ATTRIBUTE_VCOORD    "vCoord"
#define ATTRIBUTE_VTCOORD   "vtCoord"
#define ATTRIBUTE_VNCOORD   "vnCoord"
#define UNIFORM_TEXSAMPLER  "tex"
#define UNIFORM_MODELVIEW   "mMat"
#define UNIFORM_VIEWPORT    "vMat"
#define UNIFORM_PROJECTION  "pMat"

/* Controls */
#define UP    'w'
#define DOWN  's'
#define RIGHT 'd'
#define LEFT  'a'
#define SPACE 32

#endif
