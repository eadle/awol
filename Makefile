# executable files
all: testAWOL
# libraries to include
INCLUDE = -lGL -lGLU -lglut -lGLEW -lSOIL
# compiler
CC = g++
# compiler flags
CFLAGS = -Wall -std=c++0x
# object files
OBJECTS = testAWOL.o    \
          loadShaders.o \
          Mesh.o        \
          Client.o

testAWOL: $(OBJECTS) ./awol/awol.c ./awol/awol.h
	$(CC) $(OBJECTS) ./awol/awol.c $(INCLUDE) $(CFLAGS) -o testAWOL 

testAWOL.o: ./sample/testAWOL.cpp
	$(CC) -c ./sample/testAWOL.cpp

loadShaders.o: ./sample/loadShaders.c ./sample/loadShaders.h
	$(CC) -c ./sample/loadShaders.c

Client.o: ./sample/Client.cpp ./sample/Client.hpp
	$(CC) -c ./sample/Client.cpp

Mesh.o: ./sample/Mesh.cpp ./sample/Mesh.hpp
	$(CC) -c ./sample/Mesh.cpp

clean:
	rm -f testAWOL *.o
