AWOL is Another Wavefront OBJ Loader
------------------------------------
It is written entirely in C and is intended to be cross-platform and independent from both graphics and texture loading libraries. However, I have not tested it under Windows yet. It is distributed under the MIT license, so you may use it in commercial or open-source projects. If you're interested in using AWOL, I strongly suggest you take a look at the modern OpenGL sample, specifically mesh.cpp/hpp, which is acting as a C++ wrapper.

Screenshots
-----------

![Bowser's Castle](https://bitbucket.org/eadle/awol/raw/master/screenshots/bowserscastle.png)
![Waluigi Stadium](https://bitbucket.org/eadle/awol/raw/master/screenshots/waluigistadium.png)
![Luigi Circuit](https://bitbucket.org/eadle/awol/raw/master/screenshots/luigicircuit.png)

Limitations
-----------
As it currently stands, AWOL is limited to some use conditions. For one, the associated MTL file and texture files are in the same directory as the OBJ file. The following format is currently supported in the OBJ file:
~~~~
# Geometric vertices are in the format:
v float float float
# Texture coordinates are in the format:
vt float float
# Normal vertices are in the format:
vn float float float
# Faces are triangles and are exclusively in one of the following formats:
f v/vt/vn v/vt/vn v/vt/vn
f v/vt v/vt v/vt
~~~~

Running the sample application:
------------------------------
There are basic camera mechanics in place. Control movement with 'w', 's', 'a', 'd'. Press space-bar to move faster. Press 'q' to leave fullscreen.
~~~~
$ sudo apt-get install libglew-dev freeglut3-dev libsoil-dev libglm-dev
$ make
$ ./testAWOL
~~~~

Contributing
------------
AWOL is a pet project of mine and is very fragile at the moment. Should you see something that needs improvement or wish to make AWOL support more OBJ file formats, please feel free to contribute to the project! The only condition I have is that you test your modifications with valgrind.

Patch Notes
-----------
**14-01-26**
AWOL now supports OBJ files without normal vectors. usemtl and newmtl calls no longer have to be declared in the same order or have the same set cardinality. White space is skipped at the beginning of lines in MTL files. AWOL is no longer type sensitive.
